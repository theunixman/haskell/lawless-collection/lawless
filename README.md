# `liblawless`: Effects, Functional References, and Type Safety

## Submodules

### Overview

This repository contains several cabal projects using the newer [local
builds]()
## Installing

From [Version
0.25.0](http://hackage.haskell.org/package/liblawless-0.25.0), we’ve
removed our dependency on
[`libicu`](http://site.icu-project.org/). This made building using
only [`cabal`](https://www.haskell.org/cabal/) much more
difficult. Every platform had its own way of installing this, and with
all the various toolchains around, we’re leaving this in the
[`text-icu-normalized`](http://hackage.haskell.org/package/text-icu-normalized)
package, which can always be installed separately.

So, now, we’re using the new [“Nix-style local
builds”](http://blog.ezyang.com/2016/05/announcing-cabal-new-build-nix-style-local-builds/)
sof `cabal` rather than sandboxes. These are much more effective at
storing locally-built packages in a repository, and then even across
projects, only building packages which aren’t binary compatible. It
also supports projects with multiple `cabal` subprojects,
automatically adding them as dependencies. It’s well worth the
changes.

To build [`liblawless`][liblawless], you’ll need [GHC 8.0][ghc80] or
newer, [`cabal` 1.24][cabal124] or newer, and a terminal. Include
`liblawless >= 0.25 && < 0.26` in your `build-depends` field in your
`cabal` file, include our [required extensions](#required-extensions)
in the `default-extensions` field, ond then run:

    cabal new-configure
    cabal new-build

That will build your package under the `new-build/` folder,
downloading and caching anything that hasn’t been built with these
options yet. If you start a new project, and use our template `cabal`
and `cabal.project` files, it’ll reuse the same built libraries
without having to rebuild them.

## Overview

[liblawless][liblawless] is a replacement for the standard
[Prelude][prelude]. It targets [GHC 8.0][ghc80] and newer. It's core
is building a fine-grained but readily accessible Effect model to move
more type checking of code that changes its environment out of plain
IO.

[prelude]: http://hackage.haskell.org/package/base
[liblawless]: https://www.lambdanow.us/wiki/LearningProjects/liblawless
[ghc80]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/index.html

## Sample Project

We now include a skeleton project under
[`Examples/Simple`](Examples/Simple/). It includes a sample `cabal`
and `cabal.project` file for a basic library project, including all of
the required extensions, the required [liblawless][liblawless]
version, and some useful compiler flags.

## Pure vs Effectful Functions

Pure functions don't affect the anything outside of the function. A
Pure function will run a calculation on the given values, and return a
result. If you pass in the same parameters, you'll always get the same
result no matter how many times you call the function.

Effectful functions are functions that do affect the program outside
the function, and often the environment outside the computer running
them. Even with the same parameters, Effectful Functions can return
different results. In many cases they can even return signals that
completing their task wasn't possible. In many languages these are
called "Side Effects", and aren't modeled in the type system at
all. Haskell comes with a simple Effect model in its type system, the
[io][IO monad].

[io]: http://hackage.haskell.org/package/base-4.9.0.0/docs/System-IO.html#t:IO

## Kinds of Effects

The [io][IO monad] models Effects a function has on the world outside
the program. There are other, more limited Effects as well, and there
are libraries for managing these as well. The two most common are:

- [transformers][Transformers]
- [mtl][The MTL]

[transformers]: http://hackage.haskell.org/package/transformers Transformers
[mtl]: http://hackage.haskell.org/package/mtl

These model Effects that only apply to the current program, while the
IO monad models ''all other effects'' on the world. That's a really
broad brush. Most bugs in Haskell code are in the IO monad.

This project is implementing several extra types of Effects that
affect the world. Instead of treating them all the same, though, it
breaks them up into much smaller kinds of Effects. For example, for
accessing files, it's possible to:

- read bytes from a file
- write bytes to a file
- read lines of text from a file
- write lines of text from a file
- read arbitrary data from a file, operate on these data items, and
  write them to a network stream
- many other simple and complex operations

## Stream Transduction

We’re also building on the [machines][Machines] library. This library
offers a composable model for connecting strongly-typed streams
together. It also implements provisions for arbitrary effects, and
connecting Effectful functions with Pure functions into streams of
computations. When combined with the [async][Async] library, these
streams, and even individual nodes in these streams, can be run
concurrently and safely, making full use of multicore systems.

[machines]: http://hackage.haskell.org/package/machines
[async]: http://hackage.haskell.org/package/async

## Required Extensions

We make use of several language extensions to take full advantage of
stronger, simpler typing offered by the new [GHC 8.0][ghc80] type
system. As such, while our `default-extensions` list is long, the
payoff is much, much higher, especially when you factor in having so
many more useful libraries at your disposal, and much finer control
over resources, effects, and complex data structures. Our current list
of extensions is:

- ConstraintKinds
- DefaultSignatures
- DeriveDataTypeable
- DeriveGeneric
- FlexibleContexts
- FlexibleInstances
- FunctionalDependencies
- GADTs
- GeneralizedNewtypeDeriving
- KindSignatures
- LambdaCase
- MultiParamTypeClasses
- MultiWayIf
- NoImplicitPrelude
- OverloadedStrings
- PartialTypeSignatures
- RankNTypes
- RecordWildCards
- ScopedTypeVariables
- StandaloneDeriving
- TypeFamilies
- TypeOperators
- TypeSynonymInstances
- UnicodeSyntax

While they may not all be required for any single module, including
them all will make working with this package as your default prelude
much, much easier.

_NOTE:_ Since we include `NoImplicitPrelude` you’ll need to import at
least `Lawless` in every module.
