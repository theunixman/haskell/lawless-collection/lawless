# Branching and Tagging

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Branching and Tagging](#branching-and-tagging)
    - [Version Branches](#version-branches)
    - [Feature Branches](#feature-branches)
    - [Work Branches](#work-branches)

<!-- markdown-toc end -->

## Version Branches

All release branches get a _version branch_. These are the base
version, like `0.24`, followed by `-dev`. This is where upcoming
release commits are made. These branches are fast-forward only.

## Feature Branches

Each milestone has a corresponding feature branch where
possible. These branches are also fast-forard only, and should remain
rebased onto the current _version_ branches.

## Work Branches

Ideally these should start with your name, what you’re working on, and
otherwise essentially track at least one upstream branch, if not both
a _Feature_ and a _Version_ branch. Before pushing changes to a
_Feature Branch_, the changes should be rebased to clean the history
and the commit message should be clear as to what’s contained in the
changeset.
